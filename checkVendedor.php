<?php
require __DIR__.'/mgt_connection.php';

$filter = array(
	'complex_filter' => array(
		array(
			'key' => 'email',
			'value' => array('key' => 'like', 'value' => 'shopseller@pg.com')
		)
	)
);
$customerList = $client->customerCustomerList($session_id, $filter);
var_dump($customerList);

$seller_id = $customerList[0]->customer_id;
$seller = $client->customerCustomerInfo($session_id, $seller_id);
var_dump($seller);

$groups = $client->customerGroupList($session_id);
var_dump($groups);