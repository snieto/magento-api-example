<?php
require __DIR__.'/mgt_connection.php';

$quote_id = $_GET['quoteId'];

$is_product_added = $client->shoppingCartProductAdd($session_id, $quote_id, array(
	array(
		'product_id' => '905',
		'sku' => 'msj006c-Royal Blue-L',
		'qty' => '1',
		'options' => null,
		'bundle_option' => null,
		'bundle_option_qty' => null,
		'links' => null
	)
));

var_dump('Product added to cart? '.$is_product_added);

$is_product_modified = $client->shoppingCartProductUpdate($session_id, $quote_id, array(array(
	'product_id' => '905',
	'sku' => 'msj006c-Royal Blue-L',
	'qty' => '100',
	'options' => null,
	'bundle_option' => null,
	'bundle_option_qty' => null,
	'links' => null
)));

var_dump('Is product modified? '.$is_product_modified);

$is_product_deleted = $client->shoppingCartProductRemove($session_id, $quote_id, array(array(
	'product_id' => '905'
)));


var_dump('Is product deleted? '.$is_product_deleted);