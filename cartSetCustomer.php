<?php
require __DIR__.'/mgt_connection.php';

$quote_id = $_GET['quoteId'];

$customer_data = array(
	"firstname" => "testFirstname",
	"lastname" => "testLastName",
	"email" => "testEmail@mail.com",
	"mode" => "guest",
	"website_id" => "0"
);

$is_customer_set = $client->shoppingCartCustomerSet($session_id, $quote_id, $customer_data);
var_dump('is_customer_set? '.$is_customer_set);

$is_address_set = $client->shoppingCartCustomerAddresses($session_id, $quote_id, array(array(
	'mode' => 'shipping',
	'firstname' => 'first name',
	'lastname' => 'last name',
	'street' => 'C/ Poeta Querol 8 - 1',
	'city' => 'Valencia',
	'region' => 'Valencia',
	'postcode' => '46002',
	'country_id' => 'ES',
	'telephone' => '123456789',
	'is_default_billing' => 1
)));
var_dump('is_address_set shipping? '.$is_address_set);

$is_address_set = $client->shoppingCartCustomerAddresses($session_id, $quote_id, array(array(
	'mode' => 'billing',
	'firstname' => 'first name',
	'lastname' => 'last name',
	'street' => 'C/ Poeta Querol 8 - 1',
	'city' => 'Valencia',
	'region' => 'Valencia',
	'postcode' => '46002',
	'country_id' => 'ES',
	'telephone' => '123456789',
	'is_default_billing' => 1
)));
var_dump('is_address_set billing? '.$is_address_set);