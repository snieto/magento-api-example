<?php
require __DIR__.'/mgt_connection.php';

$result = $client->catalogCategoryCurrentStore($session_id, '1');
var_dump($result);
exit;
$resources = $client->resources( $session_id );
?>
<?php if( is_array( $resources ) && !empty( $resources )) { ?>
	<?php foreach( $resources as $resource ) { ?>
		<h2><?php echo $resource->title; ?></h2>
		Name: <?php echo $resource->name; ?><br/>
		Aliases: <?php echo implode( ',', $resource->aliases ); ?>
		<table>
			<tr>
				<th>Title</th>
				<th>Path</th>
				<th>Name</th>
			</tr>
			<?php foreach( $resource->methods as $method ) { ?>
				<tr>
					<td><?php echo $method->title; ?></td>
					<td><?php echo $method->path; ?></td>
					<td><?php echo $method->name; ?></td>
					<td><?php echo implode( ',', $method->aliases ); ?></td>
				</tr>
			<?php } ?>
		</table>
	<?php } ?>
<?php }
exit;

$result = $client->customerCustomerList ($session);
var_dump($result);

$result = $client->call($session, 'catalogCategoryCurrentStore');
$result = $client->call($session, 'somestuff.method', 'arg1');
$result = $client->call($session, 'somestuff.method', array('arg1', 'arg2', 'arg3'));
$result = $client->multiCall($session, array(
	array('somestuff.method'),
	array('somestuff.method', 'arg1'),
	array('somestuff.method', array('arg1', 'arg2'))
));


// If you don't need the session anymore
$client->endSession($session);